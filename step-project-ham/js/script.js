let ourServicesLinks = document.querySelector('.our-services-links');

let descArr = [
    {
        img: 'our-services-pic1.png',
        text: 'Web design text Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?'
    },
    {
        img: 'our-services-pic2.jpg',
        text: 'Graphic design text Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?'
    },
    {
        img: 'our-services-pic3.jpg',
        text: 'Online Support text Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?'
    },
    {
        img: 'our-services-pic4.jpg',
        text: 'App Design text Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?'
    },
    {
        img: 'our-services-pic5.jpg',
        text: 'Online Marketing  text Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?'
    },
    {
        img: 'our-services-pic6.jpg',
        text: 'Seo Service text Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?' +
            'Commodi ex excepturi fuga laboriosam nisi, perspiciatis praesentium veniam. Adipisci at, cum, deserunt doloribus id inventore labore libero officia quae, ut voluptas?'
    }
];

ourServicesLinks.children[0].className = "clicked-service-item";
ourServicesLinks.addEventListener('click', function (event) {
    for (let item of ourServicesLinks.children) {
        item.className = 'our-service-link-item';
    }
    event.target.className = "clicked-service-item";

    console.log(event.target.id);
    let n = event.target.id;
    document.querySelector('.description-pic').src = `img/${descArr[n - 1].img}`;
    document.querySelector('.description-text').innerHTML = `${descArr[n - 1].text}`
});


let ourAmazingLinks = document.querySelector('.our-amazing-links');


let galleryItems = document.getElementsByClassName('our-amazing-gallery-item');

let galleryArr = [

    {
        title: 'wordpress',
        img: 'wordpress/wordpress8.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress9.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design6.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design7.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design8.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design9.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design10.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design11.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design12.jpg'
    },
    {
        title: 'landing pages',
        img: 'landing/landing-page1.jpg'
    },
    {
        title: 'landing pages',
        img: 'landing/landing-page2.jpg'
    },
    {
        title: 'landing pages',
        img: 'landing/landing-page3.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design2.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design3.jpg'
    },
    {
        title: 'landing pages',
        img: 'landing/landing-page4.jpg'
    },
    {
        title: 'landing pages',
        img: 'landing/landing-page5.jpg'
    },
    {
        title: 'landing pages',
        img: 'landing/landing-page6.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design1.jpg'
    },
    {
        title: 'landing pages',
        img: 'landing/landing-page7.jpg'
    },
    {
        title: 'web design',
        img: 'web/web-design1.jpg'
    },
    {
        title: 'web design',
        img: 'web/web-design2.jpg'
    },
    {
        title: 'web design',
        img: 'web/web-design3.jpg'
    },
    {
        title: 'web design',
        img: 'web/web-design4.jpg'
    },
    {
        title: 'web design',
        img: 'web/web-design5.jpg'
    },
    {
        title: 'web design',
        img: 'web/web-design6.jpg'
    },
    {
        title: 'web design',
        img: 'web/web-design7.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress1.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress2.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design4.jpg'
    },
    {
        title: 'graphic design',
        img: 'graphic/graphic-design5.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress3.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress4.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress5.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress6.jpg'
    }, {
        title: 'wordpress',
        img: 'wordpress/wordpress7.jpg'
    },
    {
        title: 'wordpress',
        img: 'wordpress/wordpress10.jpg'
    }
];

function getPic(id) {
    switch (id) {
        case '2':
            return galleryArr.filter(elem => elem.title === 'graphic design');
            break;
        case '3':
            return galleryArr.filter(elem => elem.title === 'web design');
            break;
        case '4':
            return galleryArr.filter(elem => elem.title === 'landing pages');
            break;
        case '5':
            return galleryArr.filter(elem => elem.title === 'wordpress');
            break;
        default:
            return galleryArr;
    }
};


let loadButton = document.querySelector('.our-amazing-button');
let num = 12;
document.getElementById('a1').classList.add('clicked-amazing-link-item');

loadButton.addEventListener('click', function () {
    let clicked = document.querySelector('.clicked-amazing-link-item');
    let itemId = clicked.id.substring(1, 2);
    let arr = getPic(itemId);
    for (let i = num; i < num + 12; i++) {

        let newAmazingItem = document.createElement('div');
        let newAmazingPic = document.createElement('img');
        newAmazingItem.classList.add('our-amazing-gallery-item');
        newAmazingPic.classList.add('our-amazing-pic');

        if (typeof (arr[i]) === 'undefined') {
            newAmazingPic.src = `https://via.placeholder.com/100x100`; /* if there is no enough picture in galleryArray
                                                                         pictures will be replaced placeholders*/
        }
        else {
            newAmazingPic.src = `img/${arr[i].img}`;
        }
        newAmazingItem.appendChild(newAmazingPic);
        document.querySelector('.our-amazing-gallery-holder').appendChild(newAmazingItem);

    }

    num += 12;
    if (num >= 36) {
        loadButton.remove();
    }
    addHover();
})
;


let pics = document.getElementsByClassName('our-amazing-pic');
ourAmazingLinks.addEventListener('click', function (event) {
    let allItems = document.getElementsByClassName('our-amazing-gallery-item');
    for (let i = allItems.length - 1; i >= 12; i--) {
        allItems[i].remove();
    }


    document.querySelector('.our-amazing-holder').appendChild(loadButton);
    num = 12;
    for (let item of ourAmazingLinks.children) {
        item.className = 'our-amazing-link-item';
    }
    event.target.className = "clicked-amazing-link-item";

    let itemId = event.target.id.substring(1, 2);
    let arr = getPic(itemId);
    for (let key = 0; key < 12; key++) {
        if (typeof (arr[key]) === 'undefined') {
            pics[key].src = `https://via.placeholder.com/100x100`;
        } else {
            pics[key].src = `img/${arr[key].img}`;
        }
    }
    addHover();

});

addHover();


function addHover() {
    for (let item of galleryItems) {
        item.addEventListener('mouseenter', e => {
            const hoverContainer = document.createElement('div'),
                hoverIconShare = document.createElement('img'),
                hoverIconSearch = document.createElement('img'),
                hoverTitle = document.createElement('h4'),
                hoverCategory = document.createElement('p'),
                shareHolder = document.createElement('div'),
                searchHolder = document.createElement('div');


            hoverContainer.classList.add('gallery-hover-wrapper');
            shareHolder.classList.add('share-holder');
            searchHolder.classList.add('search-holder');
            hoverIconShare.classList.add('gallery-hover-img');
            hoverIconSearch.classList.add('gallery-hover-img');
            hoverTitle.classList.add('gallery-hover-title');
            hoverCategory.classList.add('gallery-hover-category');

            hoverIconSearch.src = 'img/iconSearch.png';
            hoverIconShare.src = 'img/iconShare.png';
            hoverTitle.innerText = 'CREATIVE DESIGN';

            let imgSrc = item.children[0].src;
            let srcIndex = imgSrc.search('img');
            if (srcIndex > 0) {
                imgSrc = imgSrc.slice(srcIndex + 4, imgSrc.length);
            } else {
                imgSrc = 'unknown';
            }

            let found = galleryArr.find(elem => elem.img === imgSrc);

            if (typeof (found) === 'undefined') {
                hoverCategory.innerText = 'unknown';
            } else {
                hoverCategory.innerText = found.title;
            }


            shareHolder.appendChild(hoverIconShare);
            searchHolder.appendChild(hoverIconSearch);

            hoverContainer.append(
                shareHolder,
                searchHolder,
                hoverTitle,
                hoverCategory
            );

            item.append(hoverContainer);
        });
        item.addEventListener('mouseleave', e => {
            item.querySelector('.gallery-hover-wrapper').remove();
        });
    }
}

















