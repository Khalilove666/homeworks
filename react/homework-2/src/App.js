import React, {Component} from 'react';
import ProductList from "./components/ProductList/ProductList";
import './App.css'

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'app'}>
                <ProductList url={'./products.json'}/>
            </div>
        );
    }
}

export default App;