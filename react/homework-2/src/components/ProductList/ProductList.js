import React, {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import './ProductList.css'
import Modal from "../AddToCartModal/Modal";

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            currentProduct: {},
            cartModal: false,
            inFavs: false
        }
    }

    componentDidMount() {
        const myReq = new XMLHttpRequest();
        myReq.open('GET', this.props.url);
        myReq.onreadystatechange = () => {
            if (myReq.readyState == 4 && myReq.status == 200) {
                this.setState({products: JSON.parse(myReq.response)});
            }
        }
        myReq.send();
    }

    openModal = (e) => {
        this.setState({
            cartModal: true,
            currentProduct: {...this.state.products[e.target.id]}
        });
    };


    addToCart = () => {
        let cartItems = [];
        if (localStorage.getItem('localCart')) {
            cartItems = JSON.parse(localStorage.getItem('localCart'));
            if (cartItems.some(item => item.product_number === this.state.currentProduct.product_number)) {
                console.log('This Product is already in cart!');
            } else {
                cartItems.push(this.state.currentProduct);
                localStorage.setItem('localCart', JSON.stringify(cartItems));
                console.log('This Product added to cart!');
            }
        } else {
            cartItems.push(this.state.currentProduct);
            localStorage.setItem('localCart', JSON.stringify(cartItems));
            console.log('This Product added to cart!');
        }
        this.setState({
            cartModal: false
        });
    };

    closeModal = (e) => {
        if (e.target == e.currentTarget) {
            this.setState({
                cartModal: false,
            })
            Object.keys(this.state.currentProduct).forEach(k => delete this.state.currentProduct[k]);
        }
    };


    addToFav = (e) => {
        let favItems = [];
        if (localStorage.getItem('localFav')) {
            favItems = JSON.parse(localStorage.getItem('localFav'));
            if (favItems.some(item => item.product_number === this.state.products[e.target.id].product_number)) {
                favItems.splice(favItems.findIndex(item => item.product_number === this.state.products[e.target.id].product_number), 1);
                localStorage.setItem('localFav', JSON.stringify(favItems));
                e.target.style.color = "#cccccc";
                console.log('This Product removed from favourites!');
            } else {
                favItems.push(this.state.products[e.target.id]);
                localStorage.setItem('localFav', JSON.stringify(favItems));
                e.target.style.color = "#ffd11a";
                console.log('This Product added to favourites!');
            }
        } else {
            favItems.push(this.state.products[e.target.id]);
            localStorage.setItem('localFav', JSON.stringify(favItems));
            e.target.style.color = "#ffd11a";
            console.log('This Product added to favourites!');
        }
    }

    render() {
        let products = this.state.products.map((item, ind) => <ProductCard index={ind} key={ind} ob={item}
                                                                           addToFav={this.addToFav}
                                                                           addToCart={this.openModal}/>);

        return (
            <div className={'products-holder'}>
                {products}
                {this.state.cartModal ? <Modal addToCart={this.addToCart} closeModal={this.closeModal}/> : null}
            </div>
        );
    }
}

export default ProductList;