import React, {Component} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

import "./App.css"

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstModalActive: false,
            secondModalActive: false,
            windowCloseBtn: false,
        }

    }

    onModalFirst = () => {
        this.setState({firstModalActive: true});

    }
    onModalSecond = () => {
        this.setState({secondModalActive: true});
    }

    closeModalFirst = () => {
        this.setState({firstModalActive: false});
    }
    closeModalSecond = () => {
        this.setState({secondModalActive: false});
    }

    bgClicked = (e) => {
        if(e.target == e.currentTarget) {
            this.setState({firstModalActive: false});
            this.setState({secondModalActive: false});
        }
    }


    render() {
        return (
            <div id={'container'} className={'container'}>
                <div>
                    <Button text={'Open first modal'} backgroundColor={'purple'} onClick={this.onModalFirst}/>
                    <Button text={'Open second modal'} backgroundColor={'pink'} onClick={this.onModalSecond}/>
                </div>

                {this.state.firstModalActive ? <Modal
                    header={'Do you want to delete this file?'}
                    text={'Once you delete this file, it will not be possible to undo this action.\nAre you sure you want to delete it?'}
                    closeButton={this.closeModalFirst}
                    bgClicked={this.bgClicked}

                /> : null}
                {this.state.secondModalActive ? <Modal
                    header={'Do you want to leave this page?'}
                    text={'Once you leave this page, all the the changes will be discarded.\nAre you sure you want to leave?'}
                    closeButton={this.closeModalSecond}
                    bgClicked={this.bgClicked}
                /> : null}
            </div>
        );
    }
}


export default App;