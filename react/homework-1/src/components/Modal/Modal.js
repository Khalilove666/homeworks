import React, {Component} from 'react';
import Cross from './Cross.png';
import './Modal.css'
import PropTypes from 'prop-types';

class Modal extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className={'modal-window-background'} onClick={this.props.bgClicked}>
                <div className={'modal-window'}>
                    <div className={'header-holder'}>
                        <p className={'header-text'}>{this.props.header}</p>
                        <button onClick={this.props.closeButton} className={'btn-cross'}><img className={'img-cross'}
                                                                                              src={Cross} alt=""/>
                        </button>
                    </div>
                    <div className={'body-holder'}>
                        <p className={'body-text'}>{this.props.text}</p>
                        <div className={'action-button-holder'}>
                            {this.props.actions}
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string
}

Modal.defaultProps = {
    actions: [
        <button className={'action-button'}>Ok</button>,
        <button className={'action-button'}>Cancel</button>
    ]
}

export default Modal;