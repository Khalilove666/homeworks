import React, {Component} from 'react';
import '.././Modal/Modal.css'
import PropTypes from 'prop-types';


class Button extends Component {
    constructor(props) {
        super(props);

    }

    buttonStyle = {
        backgroundColor: this.props.backgroundColor
    }

    render() {
        return (
            <button style={this.buttonStyle}
                    className={'modal-button'}
                    onClick={this.props.onClick}
            >
                {this.props.text}
            </button>
        );
    }
}
Button.propTypes = {
    text: PropTypes.string
}

export default Button;