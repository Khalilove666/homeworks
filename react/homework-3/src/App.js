import React, {Component} from 'react';
import {Link, Route} from "react-router-dom";
import ProductList from "./components/ProductsList/ProductList";
import CartList from "./components/CartList/CartList";
import FavouritesList from "./components/FavouritesList/FavouritesList";

import "./App.css";


class App extends Component {
  constructor(props) {
    super(props);
  }

    render() {
        return (
            <div className={'container'}>
                <header className={'header'}>
                    <Link className={'link-to-pages'} to={'/'}>Home</Link>
                    <Link className={'link-to-pages'} to={'/cart'}>Cart</Link>
                    <Link className={'link-to-pages'} to={'/favourites'}>Favourites</Link>
                </header>

                <main className={'main'}>
                    <Route path={'/'} exact={true} render = {() => <ProductList url={'./products.json'}/>} />
                    <Route path={'/cart'} exact={true} render = {() => <CartList/>} />
                    <Route path={'/favourites'} exact={true} render = {() => <FavouritesList/>} />
                </main>


            </div>

        );
    }
}

export default App;