import React, {Component} from 'react';
import './ProductCard.css';
import 'font-awesome/css/font-awesome.min.css';


class ProductCard extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={'product-card-holder'}>
                <div className={'img-holder'}>
                    <img className={'img'}
                         src={this.props.ob.path}
                         alt=""/>
                    {this.props.isCrossReal ? <button id={this.props.crossIndex} onClick={this.props.crossClicked} className={'cross-button'}><i className="fa fa-times fa-1x"></i></button> : null}
                </div>
                <div className={'product-info-holder'}>
                    <div className={'product-title-holder'}>
                        <h5 className={'product-title'}>{this.props.ob.name}</h5>
                        <i id={this.props.index} style={{color: this.props.starColor}} onClick={this.props.addToFav} className="fa fa-star fa-2x"></i>
                    </div>
                    <p className={'product-description'}>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                    <div className={'price-holder'}>
                        <span className={'price-tag'}>${this.props.ob.price}</span>
                        <button id={this.props.index} onClick={this.props.addToCart} className={'add-to-cart'}>{this.props.btnName}</button>
                    </div>
                </div>

            </div>
        );
    }
}

export default ProductCard;