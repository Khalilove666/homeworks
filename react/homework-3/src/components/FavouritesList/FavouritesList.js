import React, {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import './Favourites.css';

class FavouritesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favItems: []
        }
    }
    componentDidMount() {
        if (localStorage.getItem('localFav')) {
            this.setState({
                favItems: JSON.parse(localStorage.getItem('localFav'))
            });

        }
    }

    removeFav = (e) => {
        let favItems = [];
        if (localStorage.getItem('localFav')) {
            favItems = JSON.parse(localStorage.getItem('localFav'));
            if (favItems.some(item => item.productNumber === this.state.favItems[e.target.id].productNumber)) {
                favItems.splice(favItems.findIndex(item => item.productNumber === this.state.favItems[e.target.id].productNumber), 1);
                localStorage.setItem('localFav', JSON.stringify(favItems));
                this.setState({
                    favItems: favItems
                })
            }
        }
    }

    render() {
        let favItems = this.state.favItems.map((item, ind) => <ProductCard index={ind}
                                                            key={ind}
                                                            ob={item}
                                                            addToFav={this.removeFav}
                                                            starColor={'#ffd11a'}
                                                            btnName={'add to cart'}
                                                            isCrossReal={false}/>)
        return (
            <div className={'favourites-list'}>
                {favItems}
            </div>
        );
    }
}

export default FavouritesList;