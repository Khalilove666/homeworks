import React, {Component} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import './CartList.css'

class CartList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cartItems: []
        }
    }

    componentDidMount() {
        if (localStorage.getItem('localCart')) {
            this.setState({
                cartItems: JSON.parse(localStorage.getItem('localCart'))
            });
        }
    }
    removeFromCart = (e) => {
        let cartItems = [];
        if (localStorage.getItem('localCart')) {
            cartItems = JSON.parse(localStorage.getItem('localCart'));
            if (cartItems.some(item => item.productNumber === this.state.cartItems[e.currentTarget.id].productNumber)) {
                cartItems.splice(cartItems.findIndex(item => item.productNumber === this.state.cartItems[e.currentTarget.id].productNumber), 1);
                localStorage.setItem('localCart', JSON.stringify(cartItems));
                this.setState({
                    cartItems: cartItems
                })
            }
        }
    }


    render() {
        let cartItems = this.state.cartItems.map((item, ind) => <ProductCard crossIndex={ind}
                                                              key={ind} ob={item}
                                                              starColor={'white'}
                                                              btnName={'buy'}
                                                              isCrossReal={true}
                                                              crossClicked={this.removeFromCart}/>)
        return (
            <div className={'cart-list'}>
                {cartItems}
            </div>
        );
    }
}

export default CartList;