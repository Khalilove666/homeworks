import React, {Component} from 'react';
import './Modal.css';

class Modal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'modal-window-background'} onClick={this.props.closeModal}>
                <div className={'modal-window'}>
                    <div className={'header-holder'}>
                        <p className={'header-text'}>Do you want to add this product to cart?</p>
                    </div>
                    <div className={'body-holder'}>
                        <div className={'action-button-holder'}>
                            <button onClick={this.props.addToCart} className={'action-button'}>Add</button>
                            <button onClick={this.props.closeModal} className={'action-button'}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Modal;