

function calculator(x, y, operand) {

    switch (operand) {
        case "+":
            return (x + y);
            break;
        case "-":
            return (x - y);
            break;
        case "*":
            return (x * y);
            break;
        case "/":
            return (x / y);
            break;
        default:
            return 0;
    }
}

let num1;
let num2;
let dNum = 0;

while(!num1 || isNaN(num1)){
    let enteredNum = prompt("Enter first number: ", dNum );
    dNum = enteredNum;
    num1 = enteredNum.trim();
}
num1 = parseInt(num1);

dNum = 0;

while(!num2 || isNaN(num2)){
    let enteredNum = prompt("Enter second number: ", dNum );
    dNum = enteredNum;
    num2 = enteredNum.trim();
}
num2 = parseInt(num2);

let bool = true;
let operand;
let dOper = "+";

while(bool){
    operand = prompt("Enter mathematical operand (such as +, -, *, /)", dOper);
    dOper = operand;
    switch (operand) {
        case "+":
            bool = false;
            break;
        case "-":
            bool = false;
            break;
        case "*":
            bool = false;
            break;
        case "/":
            bool = false;
            break;
        default:
            continue;
    }
}


let result = calculator(num1, num2, operand);

console.log(result);
alert(result);



