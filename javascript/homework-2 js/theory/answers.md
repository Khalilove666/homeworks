## Answers for homeworks /javascript/homework-2 js
1. In programming we need to make same action many times. Functions allow us to write code for specific process and use it again and again.
2. Functions in programming is similar functions in mathematics. Let's imagine in mathematics we have function f(x) = x<sup>2</sup> + x + 1. In this function x will be argument of function and while calculating (or calling function) we will write values instead of arguments such as f(2) = 2<sup>2</sup> + 2+ 1.
