function createNewUser(name, surname){
    let newUser = {
        firstName: name,
        lastName: surname,
        getLogin: function(){
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        }
    };

    return newUser;
}

let user = createNewUser("Ivan", "Kravchenko");

console.log(user.getLogin());









