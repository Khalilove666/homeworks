function createNewUser(name, surname){
    let newUser = {
        firstName: name,
        lastName: surname,
        getLogin: function(){
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        setFirstName: function(newName){
            this.firstName = newName;
        },
        setLastName: function(newSurname){
            this.lastName = newSurname;
        }
    };

    return newUser;
}

let user = createNewUser("Ivan", "Kravchenko");

console.log("User before -> " + user.getLogin());


user.setFirstName("Nelson");
user.setLastName("Mandela");

console.log("User after -> " + user.getLogin());