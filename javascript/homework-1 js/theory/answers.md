## Answers for homworks/javascript/homework-1 js

1. `var` is old javascript keyword for declaring variables but now also is used. 
2. `let` is most used javascript keyword for declaring variables.
3. Difference between these two is that `var` is function scoped and `let` is block scoped. It means if you use variable before declaration will not not give error if you declared it using `var` also variables declared inside code block (such as loops) can be used outside (after) the code block if you declared it with the help of `var`. But the keyword `let` will not give you these facilities.

4. `const` usually is used for declaring constant variables (such as Pi, e) and can't be changed after setting first value. Name of `const` variables would be all uppercase. 