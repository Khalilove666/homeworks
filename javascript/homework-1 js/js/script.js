let name;
let age;

let dName = "Nelson";
let dAge = 95;

const REGEX = /^[a-zA-Z]+$/;



while(!age || isNaN(age)){
    let enteredAge = prompt("Enter your age: ", dAge);
    dAge = enteredAge;
    age = enteredAge.trim();
}

while(!name || !REGEX.test(name)){
    let enteredName = prompt("Enter your name: ", dName);
    dName = enteredName;
    name = enteredName.trim();
}

let forbidden = "You are not allowed to visit this website!";
let allowed = `Welcome, ${name}`;


if(age < 18){
    alert(forbidden);
}
else if(age >= 18 && age <= 22){
    let warn = confirm("Are you sure you want to continue?");
    if(warn === true){
        alert(allowed);
    }
    else{
        alert(forbidden);
    }
}
else{
    alert(allowed);
}
