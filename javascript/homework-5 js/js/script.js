
let linkItem = document.querySelector('.tabs');
let descriptionItems = document.querySelector('.tabs-content');


for(let oldKey in descriptionItems.children){
        descriptionItems.children[oldKey].hidden = true;  // Hide all items
    }
descriptionItems.children[0].hidden = false; // When user enters first item will be chosen as default




for(let key in linkItem.children){
    linkItem.children[key].onclick  = function () {
        for(let newKey in descriptionItems.children){
            if(newKey !== key){
                descriptionItems.children[newKey].hidden = true;
                linkItem.children[newKey].style.backgroundColor = "#23201D";
                linkItem.children[newKey].style.color = "#937341";


            }
            else {
                descriptionItems.children[newKey].hidden = false;
                linkItem.children[newKey].style.backgroundColor = "#937341";
                linkItem.children[newKey].style.color = "#23201D";
            }
        }
    }

}